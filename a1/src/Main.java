import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner numberScanner = new Scanner(System.in);
        String[] fruitsArr = new String[5];
        fruitsArr[0] = "apple";
        fruitsArr[1] = "avocado";
        fruitsArr[2] = "banana";
        fruitsArr[3] = "kiwi";
        fruitsArr[4] = "orange";
        System.out.println("Fruits in stock: "+ Arrays.toString(fruitsArr));
        System.out.println("Which fruit would you like to get the index of?");
        String searchFruit = numberScanner.nextLine();
        System.out.println("The index of "+searchFruit+ " is:"+Arrays.binarySearch(fruitsArr, searchFruit));

        ArrayList<String> friendsList = new ArrayList<>();
        friendsList.add("Diao");
        friendsList.add("Albert");
        friendsList.add("Askie");
        friendsList.add("Chuckie");
        System.out.println("My friends are:"+friendsList);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of:"+inventory);
    }
}